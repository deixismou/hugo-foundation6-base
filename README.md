
Hugo Foundation 6 Base Template
===============================

This repo contains base template files for developing static websites with
`Hugo`, and provides scaffolding and tools for working with Zurb Foundation 6.

## Quickstart Guide

execute the following commands, where `<project>` is the name of your Hugo
project:

```

$ git clone --depth 1 https://gitlab.com/deixismou/hugo-foundation6-base.git <project>

$ cd <project>

$ npm install

$ bower install

```

This will install the required dependencies and toolchain for Foundation,
including the `Gulp` taskrunner, which will automate the build process and tie
everything together. To build the source and run the Hugo development server,
execute the following command:

```
$ npm start

```

Gulp will watch the source tree for changes and recompile as required; simply
modify the source as needed and view the results in the browser.

## Project Structure

In effect, this template is very close to the Zurb Foundation 6 Stack, with
Panini replaced by Hugo for static site generation. Why? Because Hugo provides a
better way of managing site content; and I also wanted to test Zurb's claim that
Panini could be easily decoupled from the Foundation Stack... Turns out, it's
true!

Anyone who has worked with Foundation will notice a completely familiar project
structure.

```
.
├── bower.json              -- bower configuration
├── config.yaml             -- hugo configuration
├── gulpfile.babel.js       -- gulp configuration
├── LISENCE.txt             -- legalese stuff
├── package.json            -- node package configuration
├── README.md               -- you're reading me
├── public/                 -- build directory
└── source/                 -- source directory
    ├── archetypes/
    ├── assets/             -- build assets (replaces static)
    │   ├── img/
    │   ├── js/
    │   │   └── app.js          -- main application javascript
    │   └── scss/
    │       ├── app.scss        -- main application SASS
    │       ├── components/
    │       └── _settings.scss  -- Foundation settings override
    ├── content/
    │   └── post/
    │       └── hello-world.md
    ├── data/
    │   └── meta.yaml           -- site-wide metadata (title, author, etc)
    └── layouts/
        ├── _default/
        │   ├── list.html
        │   └── single.html
        ├── favicon.ico
        ├── humans.txt
        ├── index.html
        └── robots.txt
```

If you've come from using Hugo in its default configuration, this may seem new,
but all the familiar folders are still there in the source directory.  This
simply provides a better separation of source and build directories, and allows
for a neater experience with Foundation, which requires packages to be pulled
from external sources (e.g. npm and bower).

Probably the best way to think about this is to consider that, rather than
adding Foundation SASS to Hugo's project structure, this template is basically
the Foundation 6 stack with Hugo dropped in; that is, it more closely reflects
the Foundation project structure than the default Hugo arrangement.

With this in mind, it's also worth noting that the project structure here is
focused on "one-off" sites, rather than the development of re-usable Hugo
themes. However, it shouldn't be difficult to modify this base template for that
task.
